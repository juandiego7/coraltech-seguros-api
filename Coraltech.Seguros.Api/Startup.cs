﻿using AutoMapper;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Interfaces.Services;
using Coraltech.Seguros.Api.Mapping;
using Coraltech.Seguros.Api.Models;
using Coraltech.Seguros.Api.Repositories;
using Coraltech.Seguros.Api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Coraltech.Seguros.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .WithExposedHeaders("X-Pagination"));
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //ADICIONAR SERVICIO PARA ENVIAR LA CONEXION A LA BASE DE DATOS AL CONTEXTO
            services.AddDbContext<SegurosContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("segurosDB")));

            //ADICIONAR SERVICIO PARA PREVENIR bucle de referencia automática O Referencia ciclíca
            services.AddMvc().AddJsonOptions(configureJson);

            //INJECCIÓN DE DEPENDENCIAS
            services.AddTransient<IRisksService, RisksService>();
            services.AddTransient<ICoveringsService, CoveringsService>();
            services.AddTransient<IPoliciesService, PoliciesService>();
            services.AddTransient<IPoliciesCoveringsService, PoliciesCoveringsService>();
            services.AddTransient<IClientsService, ClientsService>();
            services.AddTransient<IClientsPoliciesService, ClientsPoliciesService>();

            //Inyección de dependencias (repositorios)
            services.AddScoped<IRisksRepository, RisksRepository>();
            services.AddScoped<ICoveringsRepository, CoveringsRepository>();
            services.AddScoped<IPoliciesRepository, PoliciesRepository>();
            services.AddScoped<IPoliciesCoveringsRepository, PoliciesCoveringsRepository>();
            services.AddScoped<IClientsRepository, ClientsRepository>();
            services.AddScoped<IClientsPoliciesRepository, ClientsPoliciesRepository>();

            //Inyección de dependencias (mapeos)
            services.AddSingleton<IRisksMap, RisksMap>();
            services.AddSingleton<ICoveringsMap, CoveringsMap>();
            services.AddSingleton<IPoliciesMap, PoliciesMap>();
            services.AddSingleton<IPoliciesCoveringsMap, PoliciesCoveringsMap>();
            services.AddSingleton<IClientsMap, ClientsMap>();
            services.AddSingleton<IClientsPoliciesMap, ClientsPoliciesMap>();

            //CONFIGURACIÓN DEL AUTOMAPPER VERSIÓN 8.1.1
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });

            var mapper = config.CreateMapper();

            services.AddSingleton(mapper);

        }

        //METODO PARA CONFIGURAR EL SERIALIZADO DEL JSON
        private void configureJson(MvcJsonOptions obj)
        {
            obj.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors("AllowAllOrigins");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

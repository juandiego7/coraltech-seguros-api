﻿using Coraltech.Seguros.Api.Models;
using Coraltech.Seguros.Api.Repositories;

namespace Coraltech.Seguros.Api.Interfaces.Repositories
{
    public class PoliciesRepository : Repository<Policies>, IPoliciesRepository
    {
        public PoliciesRepository(SegurosContext _dbContext) : base(_dbContext)
        {

        }
    }
}

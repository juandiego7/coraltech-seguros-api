﻿using Coraltech.Seguros.Api.Models;
using Coraltech.Seguros.Api.Repositories;

namespace Coraltech.Seguros.Api.Interfaces.Repositories
{
    public class PoliciesCoveringsRepository : Repository<PoliciesCoverings>, IPoliciesCoveringsRepository
    {
        public PoliciesCoveringsRepository(SegurosContext _dbContext) : base(_dbContext)
        {

        }
    }
}

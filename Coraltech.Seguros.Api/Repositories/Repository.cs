﻿using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Coraltech.Seguros.Api.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        #region Attributes
        private readonly SegurosContext _context;
        public Repository(SegurosContext _dbContext)
        {
            this._context = _dbContext;
        }
        #endregion

        #region Methods        
        public virtual Response GetAll(Func<IQueryable<T>, IIncludableQueryable<T, object>> includes = null)
        {
            try
            {
                // Guarda una instancia del tipo genérico y devuelve una consulta
                var query = _context.Set<T>().AsNoTracking().AsQueryable();

                // Agrega propiedades de navegación especificadas en el delegado 
                if (query != null && includes != null)
                    query = includes(query);

                return new Response { IsSuccess = true, Message = "Ok", Result = query };
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }

        public virtual Response GetObjectByCondition(Expression<Func<T, bool>> predicate, Func<IQueryable<T>, IIncludableQueryable<T, object>> includes = null)
        {
            try
            {
                // Guarda una instancia del tipo genérico y devuelve una consulta
                var query = _context.Set<T>().AsNoTracking().AsQueryable();

                // Agrega propiedades de navegación especificadas en el delegado 
                if (query != null && includes != null)
                    query = includes(query);

                // Retorna una entidad filtrada por el predicado
                var result = query.Where(predicate).FirstOrDefault();

                return new Response { IsSuccess = true, Message = "Ok", Result = result };
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }

        public virtual Response GetAllObjectsByCondition(Expression<Func<T, bool>> predicate, Func<IQueryable<T>, IIncludableQueryable<T, object>> includes = null)
        {
            try
            {
                // Guarda una instancia del tipo genérico y devuelve una consulta
                var query = _context.Set<T>().AsNoTracking().AsQueryable();

                // Agrega propiedades de navegación especificadas en el delegado 
                if (query != null && includes != null)
                    query = includes(query);

                // Retorna una entidad filtrada por el predicado
                var result = query.Where(predicate);

                return new Response { IsSuccess = true, Message = "Ok", Result = result };
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }

        public virtual Response Create(T entity)
        {
            // Modifica el estado a "Added" y devuelve un objeto diferente dependiendo del resultado
            Response response;
            try
            {
                _context.Entry(entity).State = EntityState.Added;
                var isCreated = _context.SaveChanges() > 0;

                if (isCreated)
                    response = new Response { IsSuccess = true, Message = "Ok", Result = entity };
                else
                    response = new Response { IsSuccess = false, Message = $"Error al guardar el registro {entity}" };

                return response;
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }

        public virtual Response CreateWithRelated(T entity)
        {
            // Guarda la entidad + las propiedades de navegación y devuelve un objeto diferente dependiendo del resultado
            Response response;
            try
            {
                this._context.Add(entity);
                var isCreated = _context.SaveChanges() > 0;

                if (isCreated)
                    response = new Response { IsSuccess = true, Message = "Ok", Result = entity };
                else
                    response = new Response { IsSuccess = false, Message = $"Error al guardar el registro {entity}" };

                return response;
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }

        public virtual Response Delete(T entity)
        {
            // Modifica el estado a "Deleted" y devuelve un objeto diferente dependiendo del resultado            
            Response response;
            try
            {
                _context.Entry(entity).State = EntityState.Deleted;
                var isDeleted = _context.SaveChanges() > 0;

                if (isDeleted)
                    response = new Response { IsSuccess = true, Message = "Ok", Result = entity };
                else
                    response = new Response { IsSuccess = false, Message = $"Error al eliminar el registro {entity}" };

                return response;
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }

        public virtual Response DeleteWithRelated(T entity)
        {
            // Modifica el estado a "Deleted" y devuelve un objeto diferente dependiendo del resultado            
            Response response;
            try
            {
                _context.Entry(entity).State = EntityState.Deleted;
                var isDeleted = _context.SaveChanges() > 0;

                if (isDeleted)
                    response = new Response { IsSuccess = true, Message = "Ok", Result = entity };
                else
                    response = new Response { IsSuccess = false, Message = $"Error al eliminar el registro {entity}" };

                return response;
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }

        public virtual Response Update(T entity)
        {
            // Modifica el estado a "Modified" y devuelve un objeto diferente dependiendo del resultado
            Response response;
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                var isUpdated = _context.SaveChanges() > 0;

                if (isUpdated)
                    response = new Response { IsSuccess = true, Message = "Ok", Result = entity };
                else
                    response = new Response { IsSuccess = false, Message = $"Error al actualizar el registro {entity}" };

                return response;
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }

        public virtual Response UpdateWithRelated(T entity)
        {
            // Actualiza el registro principal, si las propiedades de navegacion existen las actualiza sino crea nuevos registros 
            Response response;
            try
            {
                this._context.Update(entity);
                var isUpdated = _context.SaveChanges() > 0;

                if (isUpdated)
                    response = new Response { IsSuccess = true, Message = "Ok", Result = entity };
                else
                    response = new Response { IsSuccess = false, Message = $"Error al actualizar el registro {entity}" };

                return response;
            }
            catch (Exception exception)
            {
                return ExceptionResponse.Exception(exception);
            }
        }
        #endregion
    }
}

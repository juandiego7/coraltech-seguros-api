﻿using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Models;

namespace Coraltech.Seguros.Api.Repositories
{
    public class ClientsPoliciesRepository : Repository<ClientsPolicies>, IClientsPoliciesRepository
    {
        public ClientsPoliciesRepository(SegurosContext _dbContext) : base(_dbContext)
        {

        }
    }
}

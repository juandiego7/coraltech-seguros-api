﻿using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Models;

namespace Coraltech.Seguros.Api.Repositories
{
    public class ClientsRepository : Repository<Clients>, IClientsRepository
    {
        public ClientsRepository(SegurosContext _dbContext) : base(_dbContext)
        {

        }
    }
}

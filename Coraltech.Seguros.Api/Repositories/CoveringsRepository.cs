﻿using Coraltech.Seguros.Api.Models;
using Coraltech.Seguros.Api.Repositories;

namespace Coraltech.Seguros.Api.Interfaces.Repositories
{
    public class CoveringsRepository : Repository<Coverings>, ICoveringsRepository
    {
        public CoveringsRepository(SegurosContext _dbContext) : base(_dbContext)
        {

        }
    }
}

﻿using Coraltech.Seguros.Api.Models;
using Coraltech.Seguros.Api.Repositories;

namespace Coraltech.Seguros.Api.Interfaces.Repositories
{
    public class RisksRepository : Repository<Risks>, IRisksRepository
    {
        public RisksRepository(SegurosContext _dbContext) : base(_dbContext)
        {

        }
    }
}

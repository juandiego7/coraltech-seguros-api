﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Interfaces.Services;
using Coraltech.Seguros.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Coraltech.Seguros.Api.Services
{   
    public class RisksService : IRisksService
    {
        private ILoggerFactory _factory;
        private ILogger _logger;
        private IRisksRepository _repository;
        private IRisksMap _map;

        public RisksService(ILoggerFactory factory, IRisksRepository repository, IRisksMap map)
        {
            this._factory = factory;
            this._logger = factory.CreateLogger<RisksService>();
            _repository = repository;
            _map = map;
        }

        public Response Create(RisksDto entity)
        {
            _logger.LogDebug($"Solicitud para crear {entity}");

            var risk = _map.MapToEntity(entity);
            var response = _repository.Create(risk);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Risks)response.Result);

            return response;
        }

        public Response Delete(int id)
        {
            _logger.LogDebug("Solicitud para eliminar");
            var response = _repository.GetObjectByCondition(source => source.IdRisk == id);
            if (response.IsSuccess)
            {
                response = _repository.Delete(response.Result);
                if (response.IsSuccess)
                {
                    response.Result = _map.MapToDto((Risks)response.Result);
                }
                return response;
            }

            return response;
        }

        public Response GetAll()
        {
            _logger.LogDebug("Solicitud para listar todos");
            var response = _repository.GetAll();

            if (response.IsSuccess)
                response.Result = _map.MapToDtoList((IEnumerable<Risks>)response.Result);

            return response;
        }

        public Response GetById(int id)
        {
            _logger.LogDebug("Solicitud para buscar id");
            var response = _repository.GetObjectByCondition(
                predicate: source => source.IdRisk == id
            );

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Risks)response.Result);

            return response;
        }

        public Response Update(RisksDto entity)
        {
            _logger.LogDebug($"Solicitud para actualizar {entity}");

            var policy = _map.MapToEntity(entity);
            var response = _repository.Update(policy);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Risks)response.Result);

            return response;
        }
    }
}

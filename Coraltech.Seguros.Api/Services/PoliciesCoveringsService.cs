﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Interfaces.Services;
using Coraltech.Seguros.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Coraltech.Seguros.Api.Services
{
    public class PoliciesCoveringsService : IPoliciesCoveringsService
    {
        private ILoggerFactory _factory;
        private ILogger _logger;
        private IPoliciesCoveringsRepository _repository;
        private IPoliciesCoveringsMap _map;

        public PoliciesCoveringsService(ILoggerFactory factory, IPoliciesCoveringsRepository repository, IPoliciesCoveringsMap map)
        {
            this._factory = factory;
            this._logger = factory.CreateLogger<PoliciesCoveringsService>();
            _repository = repository;
            _map = map;
        }

        public Response Create(PoliciesCoveringsDto entity)
        {
            _logger.LogDebug($"Solicitud para crear {entity}");
             
            var policyCovering = _map.MapToEntity(entity);
            var response = _repository.Create(policyCovering);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((PoliciesCoverings)response.Result);

            return response;

        }

        public Response Delete(int idPolicy, int idCovering)
        {
            _logger.LogDebug("Solicitud para eliminar");
            var response = _repository.GetObjectByCondition(source => source.IdPolicy == idPolicy && source.IdCovering == idCovering);
            if (response.IsSuccess)
            {
                response = _repository.Delete(response.Result);
                if (response.IsSuccess)
                {
                    response.Result = _map.MapToDto((PoliciesCoverings)response.Result);
                }
                return response;
            }

            return response;
        }

        public Response GetAll()
        {
            _logger.LogDebug("Solicitud para listar todos");
            var response = _repository.GetAll();

            if (response.IsSuccess)
                response.Result = _map.MapToDtoList((IEnumerable<PoliciesCoverings>)response.Result);

            return response;
        }

        public Response GetById(int idPolicy, int idCovering)
        {
            _logger.LogDebug("Solicitud para buscar id");
            var response = _repository.GetObjectByCondition(
                predicate: source => source.IdPolicy == idPolicy && source.IdCovering == idCovering,
                includes: source => source.AsQueryable().Include(x => x.IdPolicyNavigation)
                                                        .Include(x => x.IdCoveringNavigation)
            );

            if (response.IsSuccess)
                response.Result = _map.MapToDto((PoliciesCoverings)response.Result);

            return response;
        }

        public Response Update(PoliciesCoveringsDto entity)
        {
            _logger.LogDebug($"Solicitud para actualizar {entity}");

            var policyCovering = _map.MapToEntity(entity);
            var response = _repository.Update(policyCovering);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((PoliciesCoverings)response.Result);

            return response;
        }
    }
}

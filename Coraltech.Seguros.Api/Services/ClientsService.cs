﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Interfaces.Services;
using Coraltech.Seguros.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Coraltech.Seguros.Api.Services
{
    public class ClientsService : IClientsService
    {
        private ILoggerFactory _factory;
        private ILogger _logger;
        private IClientsRepository _repository;
        private IClientsMap _map;

        public ClientsService(ILoggerFactory factory, IClientsRepository repository, IClientsMap map)
        {
            this._factory = factory;
            this._logger = factory.CreateLogger<ClientsService>();
            _repository = repository;
            _map = map;
        }

        public Response Create(ClientsDto entity)
        {
            _logger.LogDebug($"Solicitud para crear {entity}");
            var client = _map.MapToEntity(entity);
            var response = _repository.Create(client);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Clients)response.Result);

            return response;
        }

        public Response Delete(int id)
        {
            _logger.LogDebug("Solicitud para eliminar");
            var response = _repository.GetObjectByCondition(source => source.IdClient == id);
            if (response.IsSuccess)
            {
                response = _repository.Delete(response.Result);
                if (response.IsSuccess)
                {
                    response.Result = _map.MapToDto((Clients)response.Result);
                }
                return response;
            }

            return response;
        }

        public Response GetAll()
        {
            _logger.LogDebug("Solicitud para listar todos");
            var response = _repository.GetAll();

            if (response.IsSuccess)
                response.Result = _map.MapToDtoList((IEnumerable<Clients>)response.Result);

            return response;
        }

        public Response GetById(int id)
        {
            _logger.LogDebug("Solicitud para buscar id");
            var response = _repository.GetObjectByCondition(
                predicate: source => source.IdClient == id,
                 includes: source => source.AsQueryable().Include(x => x.ClientsPolicies)
                                                            .ThenInclude((ClientsPolicies y) => y.IdPolicyNavigation)    
                                                                .ThenInclude(z => z.IdRiskNavigation)
            );

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Clients)response.Result);

            return response;
        }

        public Response Update(ClientsDto entity)
        {
            _logger.LogDebug($"Solicitud para actualizar {entity}");

            var client = _map.MapToEntity(entity);
            var response = _repository.Update(client);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Clients)response.Result);

            return response;
        }

        public Response UpdateRelated(ClientsDto entity)
        {
            _logger.LogDebug($"Solicitud para actualizar {entity}");

            var client = _map.MapToEntity(entity);
            var response = _repository.UpdateWithRelated(client);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Clients)response.Result);

            return response;
        }
    }
}

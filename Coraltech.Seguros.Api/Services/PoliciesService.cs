﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Coraltech.Seguros.Api.Interfaces.Services
{
    public class PoliciesService : IPoliciesService
    {
        private ILoggerFactory _factory;
        private ILogger _logger;
        private IPoliciesRepository _repository;
        private IPoliciesMap _map;

        public PoliciesService(ILoggerFactory factory, IPoliciesRepository repository, IPoliciesMap map)
        {
            this._factory = factory;
            this._logger = factory.CreateLogger<PoliciesService>();
            _repository = repository;
            _map = map;
        }

        public Response Create(PoliciesDto entity)
        {
            _logger.LogDebug($"Solicitud para crear {entity}");
            var policy = _map.MapToEntity(entity);
            var response = _repository.Create(policy);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Policies)response.Result);

            return response;
        }

        public Response CreateRelated(PoliciesDto entity)
        {
            _logger.LogDebug($"Solicitud para crear {entity}");
            var policy = _map.MapToEntity(entity);
            var response = _repository.CreateWithRelated(policy);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Policies)response.Result);

            return response;
        }

        public Response Delete(int id)
        {
            _logger.LogDebug("Solicitud para eliminar");
            var response = _repository.GetObjectByCondition(source => source.IdPolicy == id);
            if (response.IsSuccess)
            {
                response = _repository.Delete(response.Result);
                if (response.IsSuccess)
                {
                    response.Result = _map.MapToDto((Policies)response.Result);
                }
                return response;
            }

            return response;
        }

        public Response GetAll()
        {
            _logger.LogDebug("Solicitud para listar todos");
            var response = _repository.GetAll(
                includes: source => source.AsQueryable().Include(e => e.PoliciesCoverings)                
                                                            .ThenInclude((PoliciesCoverings y) => y.IdCoveringNavigation)                                                        
                                                        .Include(e => e.IdRiskNavigation)
            );

            if (response.IsSuccess)
                response.Result = _map.MapToDtoList((IEnumerable<Policies>)response.Result);

            return response;
        }

        public Response GetById(int id)
        {
            _logger.LogDebug("Solicitud para buscar id");
            var response = _repository.GetObjectByCondition(
                predicate: source => source.IdPolicy == id,
                includes: source => source.AsQueryable().Include(e => e.IdRiskNavigation)
                                                        .Include(e => e.PoliciesCoverings)
            );

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Policies)response.Result);

            return response;
        }

        public Response Update(PoliciesDto entity)
        {
            _logger.LogDebug($"Solicitud para actualizar {entity}");

            var policy = _map.MapToEntity(entity);
            var response = _repository.Update(policy);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Policies)response.Result);

            return response;
        }

        public Response UpdateRelated(PoliciesDto entity)
        {
            _logger.LogDebug($"Solicitud para actualizar {entity}");

            var policy = _map.MapToEntity(entity);
            var response = _repository.UpdateWithRelated(policy);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Policies)response.Result);

            return response;
        }
    }
}

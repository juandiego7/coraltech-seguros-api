﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Interfaces.Services;
using Coraltech.Seguros.Api.Models;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Services
{
    public class CoveringsService : ICoveringsService
    {
        private ILoggerFactory _factory;
        private ILogger _logger;
        private ICoveringsRepository _repository;
        private ICoveringsMap _map;

        public CoveringsService(ILoggerFactory factory, ICoveringsRepository repository, ICoveringsMap map)
        {
            this._factory = factory;
            this._logger = factory.CreateLogger<CoveringsService>();
            _repository = repository;
            _map = map;
        }

        public Response Create(CoveringsDto entity)
        {
            _logger.LogDebug($"Solicitud para crear {entity}");
            var covering = _map.MapToEntity(entity);
            var response = _repository.Create(covering);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Coverings)response.Result);

            return response;
        }

        public Response Delete(int id)
        {
            _logger.LogDebug("Solicitud para eliminar");
            var response = _repository.GetObjectByCondition(source => source.IdCovering == id);
            if (response.IsSuccess)
            {
                response = _repository.Delete(response.Result);
                if (response.IsSuccess)
                {
                    response.Result = _map.MapToDto((Coverings)response.Result);
                }
                return response;
            }

            return response;
        }

        public Response GetAll()
        {
            _logger.LogDebug("Solicitud para listar todos");
            var response = _repository.GetAll();

            if (response.IsSuccess)
                response.Result = _map.MapToDtoList((IEnumerable<Coverings>)response.Result);

            return response;
        }

        public Response GetById(int id)
        {
            _logger.LogDebug("Solicitud para buscar id");
            var response = _repository.GetObjectByCondition(
                predicate: source => source.IdCovering == id
            );

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Coverings)response.Result);

            return response;
        }

        public Response Update(CoveringsDto entity)
        {
            _logger.LogDebug($"Solicitud para actualizar {entity}");

            var covering = _map.MapToEntity(entity);
            var response = _repository.Update(covering);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((Coverings)response.Result);

            return response;
        }
    }
}

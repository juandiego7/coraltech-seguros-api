﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Interfaces.Repositories;
using Coraltech.Seguros.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Coraltech.Seguros.Api.Interfaces.Services
{
    public class ClientsPoliciesService : IClientsPoliciesService
    {
        private ILoggerFactory _factory;
        private ILogger _logger;
        private IClientsPoliciesRepository _repository;
        private IClientsPoliciesMap _map;

        public ClientsPoliciesService(ILoggerFactory factory, IClientsPoliciesRepository repository, IClientsPoliciesMap map)
        {
            this._factory = factory;
            this._logger = factory.CreateLogger<ClientsPoliciesService>();
            _repository = repository;
            _map = map;
        }

        public Response Create(ClientsPoliciesDto entity)
        {
            _logger.LogDebug($"Solicitud para crear {entity}");

            var clientPolicy = _map.MapToEntity(entity);
            var response = _repository.Create(clientPolicy);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((ClientsPolicies)response.Result);

            return response;
        }

        public Response Delete(int idClient, int idPolicy)
        {
            _logger.LogDebug("Solicitud para eliminar");
            var response = _repository.GetObjectByCondition(source => source.IdClient == idClient && source.IdPolicy == idPolicy);
            if (response.IsSuccess)
            {           
                response = _repository.Delete(response.Result);
                if (response.IsSuccess)
                {
                    response.Result = _map.MapToDto((ClientsPolicies)response.Result);
                }
                return response;
            }

            return response;
        }

        public Response GetAll()
        {
            _logger.LogDebug("Solicitud para listar todos");
            var response = _repository.GetAll();

            if (response.IsSuccess)
                response.Result = _map.MapToDtoList((IEnumerable<ClientsPolicies>)response.Result);

            return response;
        }

        public Response GetById(int idClient, int idPolicy)
        {
            _logger.LogDebug("Solicitud para buscar id");
            var response = _repository.GetAllObjectsByCondition(
                predicate: source => source.IdClient == idClient && source.IdPolicy == idPolicy,
                includes: source => source.AsQueryable().Include(x => x.IdClientNavigation)
                                                        .Include(x => x.IdPolicyNavigation)
            );

            if (response.IsSuccess)
                response.Result = _map.MapToDto((ClientsPolicies)response.Result);

            return response;
        }

        public Response Update(ClientsPoliciesDto entity)
        {
            _logger.LogDebug($"Solicitud para actualizar {entity}");

            var clientPolicy = _map.MapToEntity(entity);
            var response = _repository.Update(clientPolicy);

            if (response.IsSuccess)
                response.Result = _map.MapToDto((ClientsPolicies)response.Result);

            return response;
        }
    }
}

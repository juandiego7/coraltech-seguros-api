﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Coraltech.Seguros.Api.Controllers
{

    [Route("api/v1/[controller]")]
    [ApiController]
    public class CoveringsController : Controller
    {
        private readonly ICoveringsService _service;

        public CoveringsController(ICoveringsService service)
        {
            _service = service;
        }

        // GET: api/v1/Coverings
        [HttpGet]
        public ActionResult<Response> Get()
        {
            return _service.GetAll();
        }

        // GET api/v1/Coverings/5
        [HttpGet("{id}")]
        public ActionResult<Response> GetById(int id)
        {
            return _service.GetById(id);
        }

        // POST api/v1/Coverings
        [HttpPost]
        public ActionResult<Response> Post([FromBody]CoveringsDto dto)
        {
            return _service.Create(dto);
        }

        // PUT api/v1/Coverings
        [HttpPut("{id}")]
        public ActionResult<Response> Put([FromBody]CoveringsDto dto)
        {
            return _service.Update(dto);
        }

        // DELETE api/v1/Coverings/5
        [HttpDelete("{id}")]
        public ActionResult<Response> Delete(int id)
        {
            return _service.Delete(id);
        }
    }
}

﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Coraltech.Seguros.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PoliciesController : Controller
    {
        private readonly IPoliciesService _service;

        public PoliciesController(IPoliciesService service)
        {
            _service = service;
        }

        // GET: api/v1/Policies
        [HttpGet]
        public ActionResult<Response> Get()
        {
            return _service.GetAll();
        }

        // GET api/v1/Policies/5
        [HttpGet("{id}")]
        public ActionResult<Response> GetById(int id)
        {
            return _service.GetById(id);
        }

        // POST api/v1/Policies
        [HttpPost]
        public ActionResult<Response> Post([FromBody]PoliciesDto dto)
        {
            return _service.CreateRelated(dto);
        }

        // PUT api/v1/Policies
        [HttpPut]
        public ActionResult<Response> Put([FromBody]PoliciesDto dto)
        {
            return _service.UpdateRelated(dto);
        }

        // DELETE api/v1/Policies/5
        [HttpDelete("{id}")]
        public ActionResult<Response> Delete(int id)
        {
            return _service.Delete(id);
        }
    }
}

﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Coraltech.Seguros.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PoliciesCoveringsController : Controller
    {
        private readonly IPoliciesCoveringsService _service;

        public PoliciesCoveringsController(IPoliciesCoveringsService service)
        {
            _service = service;
        }

        // GET: api/v1/PoliciesCoverings
        [HttpGet]
        public ActionResult<Response> Get()
        {
            return _service.GetAll();
        }

        // GET api/v1/PoliciesCoverings/5/3
        [HttpGet("{idPolicy}/{idCovering}")]
        public ActionResult<Response> GetById(int idPolicy, int idCovering)
        {
            return _service.GetById(idPolicy, idCovering);
        }

        // POST api/v1/PoliciesCoverings
        [HttpPost]
        public ActionResult<Response> Post([FromBody]PoliciesCoveringsDto dto)
        {
            return _service.Create(dto);
        }

        // PUT api/v1/PoliciesCoverings
        [HttpPut("{id}")]
        public ActionResult<Response> Put([FromBody]PoliciesCoveringsDto dto)
        {
            return _service.Update(dto);
        }

        // DELETE api/v1/PoliciesCoverings/5/2
        [HttpDelete("{idPolicy}/{idCovering}")]
        public ActionResult<Response> Delete(int idPolicy, int idCovering)
        {
            return _service.Delete(idPolicy, idCovering);
        }
    }
}

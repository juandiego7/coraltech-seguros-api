﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;
using Coraltech.Seguros.Api.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Coraltech.Seguros.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ClientsPoliciesController : Controller
    {
        private readonly IClientsPoliciesService _service;

        public ClientsPoliciesController(IClientsPoliciesService service)
        {
            _service = service;
        }

        // GET: api/v1/ClientsPolicies
        [HttpGet]
        public ActionResult<Response> Get()
        {
            return _service.GetAll();
        }

        // GET api/v1/ClientsPolicies/5
        [HttpGet("{idClient}/{idPolicy}")]
        public ActionResult<Response> GetById(int idClient, int idPolicy)
        {
            return _service.GetById(idClient, idPolicy);
        }

        // POST api/v1/ClientsPolicies
        [HttpPost]
        public ActionResult<Response> Post([FromBody]ClientsPoliciesDto dto)
        {
            return _service.Create(dto);
        }

        // PUT api/v1/ClientsPolicies
        [HttpPut]
        public ActionResult<Response> Put([FromBody]ClientsPoliciesDto dto)
        {
            return _service.Update(dto);
        }

        // DELETE api/v1/ClientsPolicies/5/3
        [HttpDelete("{idClient}/{idPolicy}")]
        public ActionResult<Response> Delete(int idClient, int idPolicy)
        {
            return _service.Delete(idClient, idPolicy);
        }
    }
}

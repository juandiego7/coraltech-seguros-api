﻿using Coraltech.Seguros.Api.Generic;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Coraltech.Seguros.Api.Interfaces.Repositories
{
    public interface IRepository<T> where T : class
    {
        Response GetAll(Func<IQueryable<T>, IIncludableQueryable<T, object>> includes = null);
        Response GetObjectByCondition(Expression<Func<T, bool>> predicate, Func<IQueryable<T>, IIncludableQueryable<T, object>> includes = null);
        Response GetAllObjectsByCondition(Expression<Func<T, bool>> predicate, Func<IQueryable<T>, IIncludableQueryable<T, object>> includes = null);
        Response Create(T entity);
        Response CreateWithRelated(T entity);
        Response UpdateWithRelated(T entity);
        Response Update(T entity);
        Response Delete(T entity);
    }
}

﻿using Coraltech.Seguros.Api.Models;

namespace Coraltech.Seguros.Api.Interfaces.Repositories
{
    public interface IClientsRepository : IRepository<Clients>
    {
    }
}

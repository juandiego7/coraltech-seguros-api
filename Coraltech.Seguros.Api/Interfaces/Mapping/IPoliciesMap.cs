﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coraltech.Seguros.Api.Interfaces.Mapping
{
    public interface IPoliciesMap
    {
        PoliciesDto MapToDto(Policies policy);
        Policies MapToEntity(PoliciesDto policy);
        IEnumerable<PoliciesDto> MapToDtoList(IEnumerable<Policies> policies);
    }
}

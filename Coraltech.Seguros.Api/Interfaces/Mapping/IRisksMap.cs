﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coraltech.Seguros.Api.Interfaces.Mapping
{
    public interface IRisksMap
    {
        RisksDto MapToDto(Risks risk);
        Risks MapToEntity(RisksDto risk);

        IEnumerable<RisksDto> MapToDtoList(IEnumerable<Risks> risks);
    }
}

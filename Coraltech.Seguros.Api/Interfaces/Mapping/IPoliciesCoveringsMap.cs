﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coraltech.Seguros.Api.Interfaces.Mapping
{
    public interface IPoliciesCoveringsMap
    {
        PoliciesCoveringsDto MapToDto(PoliciesCoverings policyCovering);
        PoliciesCoverings MapToEntity(PoliciesCoveringsDto policyCovering);

        IEnumerable<PoliciesCoveringsDto> MapToDtoList(IEnumerable<PoliciesCoverings> policiesCoverings);
    }
}

﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Mapping
{
    public interface IClientsMap
    {
        ClientsDto MapToDto(Clients client);
        Clients MapToEntity(ClientsDto client);

        IEnumerable<ClientsDto> MapToDtoList(IEnumerable<Clients> clients);
    }
}

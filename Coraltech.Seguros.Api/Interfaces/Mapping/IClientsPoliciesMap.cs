﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Mapping
{
    public interface IClientsPoliciesMap
    {
        ClientsPoliciesDto MapToDto(ClientsPolicies policyCovering);
        ClientsPolicies MapToEntity(ClientsPoliciesDto policyCovering);

        IEnumerable<ClientsPoliciesDto> MapToDtoList(IEnumerable<ClientsPolicies> ClientsPolicies);
    }
}

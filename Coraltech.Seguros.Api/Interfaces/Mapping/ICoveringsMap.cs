﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Mapping
{
    public interface ICoveringsMap
    {
        CoveringsDto MapToDto(Coverings covering);
        Coverings MapToEntity(CoveringsDto covering);

        IEnumerable<CoveringsDto> MapToDtoList(IEnumerable<Coverings> coverings);
    }
}

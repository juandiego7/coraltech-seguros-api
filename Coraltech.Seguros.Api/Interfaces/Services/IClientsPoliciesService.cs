﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Services
{
    public interface IClientsPoliciesService
    {
        Response GetAll();
        Response GetById(int idClient, int idPolicy);
        Response Create(ClientsPoliciesDto entity);
        Response Update(ClientsPoliciesDto entity);
        Response Delete(int idClient, int idPolicy);
    }
}

﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Services
{
    public interface IPoliciesCoveringsService
    {
        Response GetAll();
        Response GetById(int idPolicy, int idCovering);
        Response Create(PoliciesCoveringsDto entity);
        Response Update(PoliciesCoveringsDto entity);
        Response Delete(int idPolicy, int idCovering);
    }
}

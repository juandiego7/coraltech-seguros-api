﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Services
{
    public interface IRisksService
    {
        Response GetAll();
        Response GetById(int id);
        Response Create(RisksDto entity);
        Response Update(RisksDto entity);
        Response Delete(int id);
    }
}

﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Services
{
    public interface IPoliciesService
    {
        Response GetAll();
        Response GetById(int id);
        Response Create(PoliciesDto entity);
        Response CreateRelated(PoliciesDto entity);
        Response Update(PoliciesDto entity);
        Response UpdateRelated(PoliciesDto entity);
        Response Delete(int id);
    }
}

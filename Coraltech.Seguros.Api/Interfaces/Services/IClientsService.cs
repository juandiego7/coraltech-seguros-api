﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Services
{
    public interface IClientsService
    {
        Response GetAll();
        Response GetById(int id);
        Response Create(ClientsDto entity);
        Response Update(ClientsDto entity);
        Response UpdateRelated(ClientsDto entity);
        Response Delete(int id);
    }
}

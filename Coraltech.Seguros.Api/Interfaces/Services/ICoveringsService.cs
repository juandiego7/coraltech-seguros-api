﻿using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Generic;

namespace Coraltech.Seguros.Api.Interfaces.Services
{
    public interface ICoveringsService
    {
        Response GetAll();
        Response GetById(int id);
        Response Create(CoveringsDto entity);
        Response Update(CoveringsDto entity);
        Response Delete(int id);
    }
}

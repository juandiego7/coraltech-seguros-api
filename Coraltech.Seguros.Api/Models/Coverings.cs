﻿using System;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Models
{
    public partial class Coverings
    {
        public Coverings()
        {
            PoliciesCoverings = new HashSet<PoliciesCoverings>();
        }

        public int IdCovering { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PoliciesCoverings> PoliciesCoverings { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Models
{
    public partial class Clients
    {
        public Clients()
        {
            ClientsPolicies = new HashSet<ClientsPolicies>();
        }

        public int IdClient { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Document { get; set; }

        public virtual ICollection<ClientsPolicies> ClientsPolicies { get; set; }
    }
}

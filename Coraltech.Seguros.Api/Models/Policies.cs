﻿using System;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Models
{
    public partial class Policies
    {
        public Policies()
        {
            ClientsPolicies = new HashSet<ClientsPolicies>();
            PoliciesCoverings = new HashSet<PoliciesCoverings>();
        }

        public int IdPolicy { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartValidity { get; set; }
        public int? PeriodCoverage { get; set; }
        public decimal? Price { get; set; }
        public int? IdRisk { get; set; }

        public virtual Risks IdRiskNavigation { get; set; }
        public virtual ICollection<ClientsPolicies> ClientsPolicies { get; set; }
        public virtual ICollection<PoliciesCoverings> PoliciesCoverings { get; set; }
    }
}

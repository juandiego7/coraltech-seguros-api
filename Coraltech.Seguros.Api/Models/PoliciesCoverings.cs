﻿using System;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Models
{
    public partial class PoliciesCoverings
    {
        public int IdPolicy { get; set; }
        public int IdCovering { get; set; }
        public int? Coverage { get; set; }

        public virtual Coverings IdCoveringNavigation { get; set; }
        public virtual Policies IdPolicyNavigation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Models
{
    public partial class Risks
    {
        public Risks()
        {
            Policies = new HashSet<Policies>();
        }

        public int IdRisk { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Policies> Policies { get; set; }
    }
}

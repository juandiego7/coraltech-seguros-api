﻿using System;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Models
{
    public partial class ClientsPolicies
    {
        public int IdClient { get; set; }
        public int IdPolicy { get; set; }
        public bool? Active { get; set; }

        public virtual Clients IdClientNavigation { get; set; }
        public virtual Policies IdPolicyNavigation { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Coraltech.Seguros.Api.Models
{
    public partial class SegurosContext : DbContext
    {
        public SegurosContext()
        {
        }

        public SegurosContext(DbContextOptions<SegurosContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Clients> Clients { get; set; }
        public virtual DbSet<ClientsPolicies> ClientsPolicies { get; set; }
        public virtual DbSet<Coverings> Coverings { get; set; }
        public virtual DbSet<Policies> Policies { get; set; }
        public virtual DbSet<PoliciesCoverings> PoliciesCoverings { get; set; }
        public virtual DbSet<Risks> Risks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<Clients>(entity =>
            {
                entity.HasKey(e => e.IdClient);

                entity.Property(e => e.Document)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ClientsPolicies>(entity =>
            {
                entity.HasKey(e => new { e.IdClient, e.IdPolicy });

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.ClientsPolicies)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientsPolicies_Clients");

                entity.HasOne(d => d.IdPolicyNavigation)
                    .WithMany(p => p.ClientsPolicies)
                    .HasForeignKey(d => d.IdPolicy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientsPolicies_Policies");
            });

            modelBuilder.Entity<Coverings>(entity =>
            {
                entity.HasKey(e => e.IdCovering)
                    .HasName("PK_TypesCoverings");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Policies>(entity =>
            {
                entity.HasKey(e => e.IdPolicy);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.StartValidity).HasColumnType("datetime");

                entity.HasOne(d => d.IdRiskNavigation)
                    .WithMany(p => p.Policies)
                    .HasForeignKey(d => d.IdRisk)
                    .HasConstraintName("FK_Policies_Risks");
            });

            modelBuilder.Entity<PoliciesCoverings>(entity =>
            {
                entity.HasKey(e => new { e.IdPolicy, e.IdCovering })
                    .HasName("PK_PoliciesTypesRisks");

                entity.HasOne(d => d.IdCoveringNavigation)
                    .WithMany(p => p.PoliciesCoverings)
                    .HasForeignKey(d => d.IdCovering)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_PoliciesCoverings_Coverings");

                entity.HasOne(d => d.IdPolicyNavigation)
                    .WithMany(p => p.PoliciesCoverings)
                    .HasForeignKey(d => d.IdPolicy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_PoliciesCoverings_Policies");
            });

            modelBuilder.Entity<Risks>(entity =>
            {
                entity.HasKey(e => e.IdRisk)
                    .HasName("PK_TypesRisks");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(10);
            });
        }
    }
}

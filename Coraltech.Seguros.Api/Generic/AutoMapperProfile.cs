﻿using AutoMapper;
using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Models;

namespace Coraltech.Seguros.Api.Generic
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Risks, RisksDto>().ReverseMap();
            CreateMap<Coverings, CoveringsDto>().ReverseMap();
            CreateMap<Policies, PoliciesDto>().ReverseMap();
            CreateMap<PoliciesCoverings, PoliciesCoveringsDto>().ReverseMap();
            CreateMap<Clients, ClientsDto>().ReverseMap();
            CreateMap<ClientsPolicies, ClientsPoliciesDto>().ReverseMap();

        }
    }
}

﻿using System;

namespace Coraltech.Seguros.Api.Generic
{
    public static class ExceptionResponse
    {
        public static Response Exception(Exception exception)
        {
            var message = exception.Message;
            var innerException = exception.InnerException != null ? exception.InnerException.Message : "";
            var stackTrace = exception.StackTrace;
            return new Response
            {
                IsSuccess = false,
                Message = $"Message: {message}\n" +
                             $"InnerException: {innerException}\n" +
                             $"\nStackTrace: {stackTrace}",
                Result = null
            };
        }
    }
}

﻿using AutoMapper;
using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Mapping
{
    public class PoliciesMap : IPoliciesMap
    {
        private readonly IMapper _mapper;
        public PoliciesMap(IMapper mapper)
        {
            _mapper = mapper;
        }

        public PoliciesDto MapToDto(Policies policy)
        {
            return _mapper.Map<PoliciesDto>(policy);
        }

        public IEnumerable<PoliciesDto> MapToDtoList(IEnumerable<Policies> policies)
        {
            return _mapper.Map<IEnumerable<PoliciesDto>>(policies);
        }

        public Policies MapToEntity(PoliciesDto policy)
        {
            return _mapper.Map<Policies>(policy);
        }
    }
}

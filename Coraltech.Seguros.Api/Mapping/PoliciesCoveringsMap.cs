﻿using AutoMapper;
using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Mapping
{
    public class PoliciesCoveringsMap : IPoliciesCoveringsMap
    {
        private readonly IMapper _mapper;
        public PoliciesCoveringsMap(IMapper mapper)
        {
            _mapper = mapper;
        }

        public PoliciesCoveringsDto MapToDto(PoliciesCoverings policyCovering)
        {
            return _mapper.Map<PoliciesCoveringsDto>(policyCovering);
        }

        public IEnumerable<PoliciesCoveringsDto> MapToDtoList(IEnumerable<PoliciesCoverings> policiesCoverings)
        {
            return _mapper.Map<IEnumerable<PoliciesCoveringsDto>>(policiesCoverings);
        }

        public PoliciesCoverings MapToEntity(PoliciesCoveringsDto policyCovering)
        {
            return _mapper.Map<PoliciesCoverings>(policyCovering);
        }
    }
}

﻿using AutoMapper;
using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Mapping
{
    public class ClientsPoliciesMap : IClientsPoliciesMap
    {
        private readonly IMapper _mapper;
        public ClientsPoliciesMap(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ClientsPoliciesDto MapToDto(ClientsPolicies clientPolicy)
        {
            return _mapper.Map<ClientsPoliciesDto>(clientPolicy);
        }

        public IEnumerable<ClientsPoliciesDto> MapToDtoList(IEnumerable<ClientsPolicies> clientsPolicies)
        {
            return _mapper.Map<IEnumerable<ClientsPoliciesDto>>(clientsPolicies);
        }

        public ClientsPolicies MapToEntity(ClientsPoliciesDto clientPolicy)
        {
            return _mapper.Map<ClientsPolicies>(clientPolicy);
        }
    }
}

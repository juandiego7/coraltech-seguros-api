﻿using AutoMapper;
using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Mapping
{
    public class RisksMap : IRisksMap
    {
        private readonly IMapper _mapper;
        public RisksMap(IMapper mapper)
        {
            _mapper = mapper;
        }

        public RisksDto MapToDto(Risks risk)
        {
            return _mapper.Map<RisksDto>(risk);
        }

        public IEnumerable<RisksDto> MapToDtoList(IEnumerable<Risks> risks)
        {
            return _mapper.Map<IEnumerable<RisksDto>>(risks);
        }

        public Risks MapToEntity(RisksDto risk)
        {
            return _mapper.Map<Risks>(risk);
        }
    }
}

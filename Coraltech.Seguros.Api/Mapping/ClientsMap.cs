﻿using AutoMapper;
using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Mapping
{
    public class ClientsMap : IClientsMap
    {
        private readonly IMapper _mapper;
        public ClientsMap(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ClientsDto MapToDto(Clients client)
        {
            return _mapper.Map<ClientsDto>(client);
        }

        public IEnumerable<ClientsDto> MapToDtoList(IEnumerable<Clients> clients)
        {
            return _mapper.Map<IEnumerable<ClientsDto>>(clients);
        }

        public Clients MapToEntity(ClientsDto client)
        {
            return _mapper.Map<Clients>(client);
        }
    }
}

﻿using AutoMapper;
using Coraltech.Seguros.Api.Dto;
using Coraltech.Seguros.Api.Interfaces.Mapping;
using Coraltech.Seguros.Api.Models;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Mapping
{
    public class CoveringsMap : ICoveringsMap
    {
        private readonly IMapper _mapper;
        public CoveringsMap(IMapper mapper)
        {
            _mapper = mapper;
        }

        public CoveringsDto MapToDto(Coverings covering)
        {
            return _mapper.Map<CoveringsDto>(covering);
        }

        public IEnumerable<CoveringsDto> MapToDtoList(IEnumerable<Coverings> coverings)
        {
            return _mapper.Map<IEnumerable<CoveringsDto>>(coverings);
        }

        public Coverings MapToEntity(CoveringsDto covering)
        {
            return _mapper.Map<Coverings>(covering);
        }
    }
}

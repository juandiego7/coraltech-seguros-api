﻿namespace Coraltech.Seguros.Api.Dto
{
    public class ClientsPoliciesDto
    {
        public int IdClient { get; set; }
        public int IdPolicy { get; set; }
        public bool? Active { get; set; }

        public ClientsDto IdClientNavigation { get; set; }
        public PoliciesDto IdPolicyNavigation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Dto
{
    public class PoliciesDto
    {
        public int IdPolicy { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartValidity { get; set; }
        public int? PeriodCoverage { get; set; }
        public decimal? Price { get; set; }
        public int? IdRisk { get; set; }

        public RisksDto IdRiskNavigation { get; set; }
        public IList<PoliciesCoveringsDto> PoliciesCoverings { get; set; }
    }
}

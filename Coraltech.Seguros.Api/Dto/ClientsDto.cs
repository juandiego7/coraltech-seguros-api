﻿using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Dto
{
    public class ClientsDto
    {
        public int IdClient { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Document { get; set; }

        public  IList<ClientsPoliciesDto> ClientsPolicies { get; set; }

    }
}

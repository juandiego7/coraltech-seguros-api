﻿namespace Coraltech.Seguros.Api.Dto
{
    public class PoliciesCoveringsDto
    {
        public int IdPolicy { get; set; }
        public int IdCovering { get; set; }
        public int? Coverage { get; set; }

        public CoveringsDto IdCoveringNavigation { get; set; }
        public PoliciesDto IdPolicyNavigation { get; set; }
    }
}

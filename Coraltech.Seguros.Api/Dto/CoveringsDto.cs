﻿using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Dto
{
    public class CoveringsDto
    {
        public int IdCovering { get; set; }
        public string Name { get; set; }

        public  IList<PoliciesCoveringsDto> PoliciesCoverings { get; set; }
    }
}

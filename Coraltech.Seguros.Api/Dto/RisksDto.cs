﻿using System.Collections.Generic;

namespace Coraltech.Seguros.Api.Dto
{
    public partial class RisksDto
    {    
        public int IdRisk { get; set; }
        public string Name { get; set; }

        public IList<PoliciesDto> Policies { get; set; }
    }
}
